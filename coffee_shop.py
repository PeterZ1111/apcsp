class CoffeeShop:
    def __init__(self, name):
        self.name = name
        self.drinks = []

    def add_drink(self, name, price):
        drink = {"name": name, "price": price}
        self.drinks.append(drink)
        print(f"Added {name} to the menu.")

    def remove_drink(self, name):
        for drink in self.drinks:
            if drink["name"] == name:
                self.drinks.remove(drink)
                print(f"Removed {name} from the menu.")
                return
        print(f"{name} is not available on the menu.")

    def view_menu(self):
        if not self.drinks:
            print("No drinks available on the menu.")
        else:
            print("Menu:")
            for drink in self.drinks:
                print(f"{drink['name']}: ${drink['price']}")

# Create a coffee shop instance
coffee_shop = CoffeeShop("My Coffee Shop")

coffee_shop.add_drink("Americano", 8.0)
coffee_shop.add_drink("Cappuccino", 10.0)
coffee_shop.add_drink("Latte", 12.0)

coffee_shop.view_menu()

coffee_shop.remove_drink("Latte")

coffee_shop.view_menu()