import random

first_names = ["Alice", "Bob", "Charlie", "Dog", "Pig"]

last_names = ["Smith", "Johnson", "Rabbit", "Dragon", "Brown"]

while True:
    first_name = random.choice(first_names)
    last_name = random.choice(last_names)
    print(first_name, last_name)
    answer = input("Play again? (y/n) ")
    if answer.lower() == "n":
        break