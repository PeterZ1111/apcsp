def pig_latin_undo(string):
    words = string.split()
    english_words = []
    for word in words:
        if word[-3:] == 'ay':
            english_word = word[:-3]
        else:
            english_word = word[-3] + word[:-3][-len(word):-3]
        english_words.append(english_word)
    return ' '.join(english_words)

pig_latin_string = input("Enter a string of words in Pig Latin: ")

print(pig_latin_undo(pig_latin_string))