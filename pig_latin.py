def pig_latin(string):
    words = string.split()
    pig_latin_words = []
    for word in words:
        if len(word) <= 3:
            pig_latin_word = word
        elif word[0] in "aeiouAEIOU":
            pig_latin_word = word + "way"
        else:
            pig_latin_word = word[1:] + word[0] + "ay"
        pig_latin_words.append(pig_latin_word)
    return ' '.join(pig_latin_words)

english_string = input("Enter a string of words: ")
pig_latin_string = pig_latin(english_string)
print(pig_latin_string)